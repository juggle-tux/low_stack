#ifndef INTSTACK_H
#define INTSTACK_H
#include <iostream>
using namespace std;

class IntStack
{
    private:
        int *stackArray;
        int stackSize;
        int top;

    public:
        IntStack(int size)
        {
            stackArray = new int[size];
            stackSize = size;
            top = -1;
            cout << "Message From Constructor: Initializing a stack with the size being " << stackSize << endl;
        }
        ~IntStack()
        {
            delete [] stackArray;
        }
        void push(int);
        void pop(int &);
        bool isFull(void);
        bool isEmpty(void);
        int getTop(void){
            return top;
        }
        int getStackSize(void){
            return stackSize;
        }
        int printStack(int);
};

#endif
