#include <iostream>
#include "intstack.hpp"

using namespace std;

int main(int argc, char** argv) {
    IntStack stack(5);

    int catchVar;
    cout << "Pushing 5\n";
    stack.push(5);
    cout << "Pushing 10\n";
    stack.push(10);
    cout << "Pushing 15\n";
    stack.push(15);
    cout << "Pushing 20\n";
    stack.push(20);
    cout << "Pushing 500\n";
    stack.push(500);
    cout << "Pushing a value that will cause stackoverflow error\n";
    stack.push(1000);
    cout << "Popping...\n";
    stack.pop(catchVar);
    cout << catchVar << endl;
    cout << "Popping...\n";
    stack.pop(catchVar);
    cout << catchVar << endl;

    //Display top item from Stack

//    cout << "The top element is " << stack.printTop() << endl;
//    cout << "The stack size is " << stack.printStackSize() << endl;

    int stckSize = stack.getStackSize();
    int counter = 0;
    for(counter; counter < stckSize; counter++)
    {
        stack.printStack(counter);
    }



    return 0;
}
