#include <iostream>
#include "intstack.hpp"
using namespace std;

void IntStack::push(int num)
{
    if(isFull())
    {
	  cout << "The stack is full.\n";
    }
    else
    {
        top++;
        stackArray[top] = num;
    }
}

void IntStack::pop(int &num){
    if (isEmpty())
    {
        cout << "The stack is empty.\n";
    }
    else
    {
        num = stackArray[top];
        top--;
    }
}

bool IntStack::isFull(void){
    bool status;

        if(top == stackSize - 1)
        status = true;
        else
        status = false;

        return status;
}

bool IntStack::isEmpty(void)
{
    bool status;

    if(top == -1)
        status = true;
    else
        status = false;

    return status;
}

int IntStack::printStack(int index)
{
        //cout << stackArray[index] << endl;
        cout << "index is " << index << " and the value at index is " << stackArray[index]  <<endl;
        //cout << stackArray[index];
}
